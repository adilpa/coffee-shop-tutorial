"use strict";

module.exports = function (Coffeeshop) {
  /**
   * API to create coffeshop
   * @param {string} coffeeShopName Coffeeshop name
   * @param {string} address Address of the coffeeshop
   * @param {string} phoneNumber Phone number of coffeeshop
   * @param {Function(Error, object)} callback
   */

  Coffeeshop.saveCoffeeshop = async function (
    coffeeShopName,
    address,
    phoneNumber
  ) {
    try {
      const coffeshop = await Coffeeshop.create({
        name: coffeeShopName,
        address: address,
        phoneNumber: phoneNumber,
      });
      return coffeshop;
    } catch (error) {
      throw error;
    }
  };
};
